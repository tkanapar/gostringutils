package gostringutils

import "testing"

func TestReverse(t *testing.T) {
	cases := []struct {
		input, expected string
	}{
		{"Hello, World", "dlroW ,olleH"},
		{"", ""},
		{"abc123", "321cba"},
	}
	for _, c := range cases {
		result := Reverse(c.input)
		if result != c.expected {
			t.Errorf("Reverse(%q) != %q, expected %q", c.input, result, c.expected)
		}
	}
}
